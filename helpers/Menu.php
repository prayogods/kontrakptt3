<?php
/**
 * Menu Items
 * All Project Menu
 * @category  Menu List
 */

class Menu{
	
	
	public static $navbarsideleft = array(
		array(
			'path' => 'home', 
			'label' => 'Home', 
			'icon' => '<i class="fa fa-dashboard "></i>'
		),
		
		array(
			'path' => 'instansi', 
			'label' => 'Instansi', 
			'icon' => '<i class="fa fa-dashcube "></i>'
		),
		
		array(
			'path' => 'pasal', 
			'label' => 'Pasal', 
			'icon' => '<i class="fa fa-adn "></i>'
		),
		
		array(
			'path' => 'pejabat', 
			'label' => 'Pejabat', 
			'icon' => '<i class="fa fa-black-tie "></i>'
		),
		
		array(
			'path' => 'data_ptt', 
			'label' => 'Input Profil PTT', 
			'icon' => '<i class="fa fa-child "></i>'
		),
		
		array(
			'path' => 'akun', 
			'label' => 'Akun', 
			'icon' => '<i class="fa fa-drupal "></i>'
		)
	);

	
	
	public static $INSTANSI = array();

	public static $ROLE = array(
		array(
			"value" => "Administrator", 
			"label" => "Administrator", 
		),
		array(
			"value" => "User", 
			"label" => "User", 
		),
		array(
			"value" => "PPK", 
			"label" => "PPK", 
		),);

}