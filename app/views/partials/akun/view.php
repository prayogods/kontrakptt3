
<?php 
//check if current user role is allowed access to the pages
$can_add = PageAccessManager::is_allowed('akun/add');
$can_edit = PageAccessManager::is_allowed('akun/edit');
$can_view = PageAccessManager::is_allowed('akun/view');
$can_delete = PageAccessManager::is_allowed('akun/delete');
?>

<?php
$comp_model = new SharedController;

$csrf_token = Csrf :: $token;

//Page Data Information from Controller
$data = $this->view_data;

//$rec_id = $data['__tableprimarykey'];
$page_id = Router::$page_id; //Page id from url

$view_title = $this->view_title;

$show_header = $this->show_header;
$show_edit_btn = $this->show_edit_btn;
$show_delete_btn = $this->show_delete_btn;
$show_export_btn = $this->show_export_btn;

?>

<section class="page">
    
    <?php
    if( $show_header == true ){
    ?>
    
    <div  class="bg-light p-3 mb-3">
        <div class="container">
            
            <div class="row ">
                
                <div class="col-12 ">
                    <h3 class="record-title">View  Akun</h3>
                    
                </div>
                
            </div>
        </div>
    </div>
    
    <?php
    }
    ?>
    
    <div  class="">
        <div class="container">
            
            <div class="row ">
                
                <div class="col-md-12 comp-grid">
                    
                    <?php $this :: display_page_errors(); ?>
                    
                    <div  class=" animated fadeIn">
                        <div class="profile-bg mb-2">
                            <div class="profile">
                                <div class="d-flex flex-row justify-content-center">
                                    <div class="avatar">
                                        <?php 
                                        if(!empty($data['PHOTO'])){
                                        Html::page_img($data['PHOTO'],100,100); 
                                        }
                                        ?>
                                    </div>
                                    <h2 class="title"><?php echo $data['USERNAME']; ?></h2>
                                </div>
                            </div>
                        </div>
                        <div>
                            <table class="table table-hover table-borderless table-striped">
                                <tbody>
                                    
                                    <tr>
                                        <th class="title"> Id :</th>
                                        <td class="value"> <?php echo $data['ID']; ?> </td>
                                    </tr>
                                    
                                    
                                    <tr>
                                        <th class="title"> Username :</th>
                                        <td class="value"> <?php echo $data['USERNAME']; ?> </td>
                                    </tr>
                                    
                                    
                                    <tr>
                                        <th class="title"> Email :</th>
                                        <td class="value"> <?php echo $data['EMAIL']; ?> </td>
                                    </tr>
                                    
                                    
                                    <tr>
                                        <th class="title"> Role :</th>
                                        <td class="value"> <?php echo $data['ROLE']; ?> </td>
                                    </tr>
                                    
                                    
                                    <tr>
                                        <th class="title"> Nomorktp :</th>
                                        <td class="value"> <?php echo $data['NOMORKTP']; ?> </td>
                                    </tr>
                                    
                                    
                                    <tr>
                                        <th class="title"> Nomorhp :</th>
                                        <td class="value"> <?php echo $data['NOMORHP']; ?> </td>
                                    </tr>
                                    
                                    
                                </tbody>    
                            </table>    
                        </div>  
                        <div class="mt-2">
                            
                            <?php if($can_edit){ ?>
                            
                            <a class="btn btn-sm btn-info"  href="<?php print_link("akun/edit/$rec_id"); ?>">
                                <i class="fa fa-edit"></i> 
                            </a>
                            
                            <?php } ?>
                            
                            
                            <?php if($can_delete){ ?>
                            
                            <a class="btn btn-sm btn-danger record-delete-btn"  href="<?php print_link("akun/delete/$rec_id/?csrf_token=$csrf_token"); ?>" data-prompt-msg="Yakin Ingin Dihapus ?" data-display-style="modal">
                                <i class="fa fa-times"></i> 
                            </a>
                            
                            <?php } ?>
                            
                        </div>
                    </div>
                    
                </div>
                
            </div>
        </div>
    </div>
    
</section>
