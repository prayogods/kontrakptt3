
<?php 
//check if current user role is allowed access to the pages
$can_add = PageAccessManager::is_allowed('data_ptt/add');
$can_edit = PageAccessManager::is_allowed('data_ptt/edit');
$can_view = PageAccessManager::is_allowed('data_ptt/view');
$can_delete = PageAccessManager::is_allowed('data_ptt/delete');
?>

<?php
$comp_model = new SharedController;

$csrf_token = Csrf :: $token;

//Page Data Information from Controller
$data = $this->view_data;

//$rec_id = $data['__tableprimarykey'];
$page_id = Router::$page_id; //Page id from url

$view_title = $this->view_title;

$show_header = $this->show_header;
$show_edit_btn = $this->show_edit_btn;
$show_delete_btn = $this->show_delete_btn;
$show_export_btn = $this->show_export_btn;

?>

<section class="page">
    
    <?php
    if( $show_header == true ){
    ?>
    
    <div  class="bg-light p-3 mb-3">
        <div class="container">
            
            <div class="row ">
                
                <div class="col-12 ">
                    <h3 class="record-title">View  Data Ptt</h3>
                    
                </div>
                
            </div>
        </div>
    </div>
    
    <?php
    }
    ?>
    
    <div  class="">
        <div class="container">
            
            <div class="row ">
                
                <div class="col-md-12 comp-grid">
                    
                    <?php $this :: display_page_errors(); ?>
                    
                    <div  class=" animated fadeIn">
                        <?php
                        
                        $counter = 0;
                        if(!empty($data)){
                        $rec_id = (!empty($data['ID']) ? urlencode($data['ID']) : null);
                        
                        
                        
                        $counter++;
                        ?>
                        <div class="page-records ">
                            <table class="table table-hover table-borderless table-striped">
                                <!-- Table Body Start -->
                                <tbody>
                                    
                                    <tr>
                                        <th class="title"> Id :</th>
                                        <td class="value"> <?php echo $data['ID']; ?> </td>
                                    </tr>
                                    
                                    
                                    <tr>
                                        <th class="title"> Username :</th>
                                        <td class="value"> <?php echo $data['USERNAME']; ?> </td>
                                    </tr>
                                    
                                    
                                    <tr>
                                        <th class="title"> Nomorktp :</th>
                                        <td class="value"> <?php echo $data['NOMORKTP']; ?> </td>
                                    </tr>
                                    
                                    
                                    <tr>
                                        <th class="title"> Tempatlahir :</th>
                                        <td class="value"> <?php echo $data['TEMPATLAHIR']; ?> </td>
                                    </tr>
                                    
                                    
                                    <tr>
                                        <th class="title"> Tanggallahir :</th>
                                        <td class="value"> <?php echo $data['TANGGALLAHIR']; ?> </td>
                                    </tr>
                                    
                                    
                                    <tr>
                                        <th class="title"> Nohp :</th>
                                        <td class="value"> <?php echo $data['NOHP']; ?> </td>
                                    </tr>
                                    
                                    
                                    <div><?php echo $data['ALAMAT']; ?></div>
                                    
                                    
                                    
                                    <tr>
                                        <th class="title"> Pendidikan :</th>
                                        <td class="value"> <?php echo $data['PENDIDIKAN']; ?> </td>
                                    </tr>
                                    
                                    
                                    <tr>
                                        <th class="title"> Fotoktp :</th>
                                        <td class="value"><?php Html :: page_img($data['FOTOKTP'],400,400,1); ?></td>
                                    </tr>
                                    
                                    
                                    <tr>
                                        <th class="title"> Sertifikat :</th>
                                        <td class="value"><?php Html :: page_img($data['SERTIFIKAT'],400,400,1); ?></td>
                                    </tr>
                                    
                                    
                                    <tr>
                                        <th class="title"> Photo :</th>
                                        <td class="value"><?php Html :: page_img($data['PHOTO'],400,400,1); ?></td>
                                    </tr>
                                    
                                    
                                    <tr>
                                        <th class="title"> Ijasah :</th>
                                        <td class="value"><?php Html :: page_img($data['IJASAH'],400,400,1); ?></td>
                                    </tr>
                                    
                                    
                                    <tr>
                                        <th class="title"> Kk :</th>
                                        <td class="value"><?php Html :: page_img($data['KK'],400,400,1); ?></td>
                                    </tr>
                                    
                                    
                                    <tr>
                                        <th class="title"> Npwp :</th>
                                        <td class="value"><?php Html :: page_img($data['NPWP'],400,400,1); ?></td>
                                    </tr>
                                    
                                    
                                    <tr>
                                        <th class="title"> Instansi :</th>
                                        <td class="value"> <?php echo $data['INSTANSI']; ?> </td>
                                    </tr>
                                    
                                    
                                    <tr>
                                        <th class="title"> Nomorkontrak :</th>
                                        <td class="value"> <?php echo $data['nomorkontrak']; ?> </td>
                                    </tr>
                                    
                                    
                                    <tr>
                                        <th class="title"> Kontraklama :</th>
                                        <td class="value"> <?php echo $data['kontraklama']; ?> </td>
                                    </tr>
                                    
                                    
                                    <tr>
                                        <th class="title"> Masakerja :</th>
                                        <td class="value"> <?php echo $data['MASAKERJA']; ?> </td>
                                    </tr>
                                    
                                    
                                </tbody>
                                <!-- Table Body End -->
                                <tfoot>
                                    <tr>
                                        
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                        <div class="p-3">
                            
                            
                            <?php if($can_edit){ ?>
                            
                            <a class="btn btn-sm btn-info"  href="<?php print_link("data_ptt/edit/$rec_id"); ?>">
                                <i class="fa fa-edit"></i> 
                            </a>
                            
                            <?php } ?>
                            
                            
                            <?php if($can_delete){ ?>
                            
                            <a class="btn btn-sm btn-danger record-delete-btn"  href="<?php print_link("data_ptt/delete/$rec_id/?csrf_token=$csrf_token"); ?>" data-prompt-msg="Yakin Ingin Dihapus" data-display-style="modal">
                                <i class="fa fa-times"></i> 
                            </a>
                            
                            <?php } ?>
                            
                            
                            <button class="btn btn-sm btn-primary export-btn">
                                <i class="fa fa-save"></i> 
                            </button>
                            
                            
                        </div>
                        <?php
                        }
                        else{
                        ?>
                        <!-- Empty Record Message -->
                        <div class="text-muted p-3">
                            <i class="fa fa-ban"></i> No Record Found
                        </div>
                        <?php
                        }
                        ?>
                    </div>
                    
                    <a  class="btn btn-primary" href="<?php print_link("data_ptt/list") ?>">
                        
                        Kembali 
                    </a>
                    
                </div>
                
            </div>
        </div>
    </div>
    
</section>
