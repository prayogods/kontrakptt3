
<?php 
//check if current user role is allowed access to the pages
$can_add = PageAccessManager::is_allowed('data_ptt/add');
$can_edit = PageAccessManager::is_allowed('data_ptt/edit');
$can_view = PageAccessManager::is_allowed('data_ptt/view');
$can_delete = PageAccessManager::is_allowed('data_ptt/delete');
?>

<?php

$comp_model = new SharedController;

$csrf_token = Csrf :: $token;

//Page Data From Controller
$view_data = $this->view_data;

$records = $view_data->records;
$record_count = $view_data->record_count;
$total_records = $view_data->total_records;

$field_name = Router :: $field_name;
$field_value = Router :: $field_value;

$view_title = $this->view_title;
$show_header = $this->show_header;
$show_footer = $this->show_footer;
$show_pagination = $this->show_pagination;


?>

<section class="page">
    
    <?php
    if( $show_header == true ){
    ?>
    
    <div  class="bg-light p-3 mb-3">
        <div class="container-fluid">
            
            <div class="row ">
                
                <div class="col-sm-4 ">
                    <h3 class="record-title">Data Ptt</h3>
                    
                </div>
                
                <div class="col-sm-3 ">
                    
                    <?php if($can_add){ ?>
                    
                    <a  class="btn btn btn-primary my-1" href="<?php print_link("data_ptt/add") ?>">
                        <i class="fa fa-plus"></i>                              
                        Buat Baru Profil PTT 
                    </a>
                    
                    <?php } ?>
                    
                </div>
                
                <div class="col-sm-5 ">
                    
                    <form method="get" action="" class="form">
                        
                        <form  class="search" method="get">
                            <div class="input-group">
                                <input value="<?php echo get_query_str_value('search'); ?>" class="form-control" type="text" name="search"  placeholder="Search" />
                                    <div class="input-group-append">
                                        <button class="btn btn-primary"><i class="fa fa-search"></i></button>
                                    </div>
                                </div>
                            </form>
                            
                            <div class="dropdown">
                                <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Drop Down
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    
                                    
                                    <?php 
                                    $option_list = $comp_model->data_ptt_data_pttINSTANSI_option_list();
                                    if(!empty($option_list)){
                                    foreach($option_list as $arr){
                                    $val = array_values($arr);
                                    $nav_link = set_current_page_link(array('data_ptt_INSTANSI' => $val[0]) , false);
                                    ?>
                                    <a class="dropdown-item <?php echo is_active_link('data_ptt_INSTANSI', $val[0]); ?>" href="<?php print_link($nav_link) ?>">
                                        <?php echo (!empty($val[1]) ? $val[1] : $val[0]); ?>
                                    </a>
                                    <?php
                                    }
                                    }
                                    ?>
                                    
                                </div>
                            </div>
                            
                            <hr />
                            <div class="form-group text-center">
                                <button class="btn btn-primary">Filter</button>
                            </div>
                        </form>
                        
                    </div>
                    
                    <div class="col-md-12 comp-grid">
                        <div class="">
                            <?php
                            if(!empty($field_name) || !empty($_GET['search'])){
                            ?>
                            <hr class="sm d-block d-sm-none" />
                            <nav class="page-header-breadcrumbs mt-2" aria-label="breadcrumb">
                                <ul class="breadcrumb m-0 p-1">
                                    <?php
                                    if(!empty($field_name)){
                                    ?>
                                    <li class="breadcrumb-item"><a class="text-capitalize" href="<?php print_link('data_ptt') ?>"><?php echo $field_name ?></a></li>
                                    <li  class="breadcrumb-item active text-capitalize"><?php echo urldecode($field_value) ?></li>
                                    <?php 
                                    }   
                                    ?>
                                    
                                    <?php
                                    if(!empty($_GET['search'])){
                                    ?>
                                    <li class="breadcrumb-item">
                                        <a class="text-capitalize" href="<?php print_link('data_ptt') ?>">Search</a>
                                    </li>
                                    <li  class="breadcrumb-item active text-capitalize"> <strong><?php echo get_value('search'); ?></strong></li>
                                    <?php
                                    }
                                    ?>
                                    
                                </ul>
                            </nav>  
                            <?php
                            }
                            ?>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
        
        <?php
        }
        ?>
        
        <div  class="">
            <div class="container-fluid">
                
                <div class="row ">
                    
                    <div class="col-md-12 comp-grid">
                        
                        <?php $this :: display_page_errors(); ?>
                        <div class="filter-tags mb-2">
                            <?php
                            if(!empty($_GET['data_ptt_INSTANSI'])){
                            ?>
                            <div class="filter-chip card bg-light">
                                <b>Data Ptt Instansi :</b> <?php echo get_value('data_ptt_INSTANSI'); ?>
                                <a href="<?php print_link(unset_page_querystring('data_ptt_INSTANSI')); ?>" class="close-btn">
                                    &times;
                                </a>
                            </div>
                            <?php
                            }
                            ?>
                        </div>
                        <div  class=" animated fadeIn">
                            <div id="data_ptt-list-records">
                                
                                <?php
                                if(!empty($records)){
                                ?>
                                <div class="page-records table-responsive">
                                    <table class="table  table-striped table-sm">
                                        <thead class="table-header bg-light">
                                            <tr>
                                                
                                                <?php if($can_delete){ ?>
                                                
                                                <th class="td-sno td-checkbox">
                                                    <label class="custom-control custom-checkbox custom-control-inline">
                                                        <input class="toggle-check-all custom-control-input" type="checkbox" />
                                                        <span class="custom-control-label"></span>
                                                    </label>
                                                </th>
                                                
                                                <?php } ?>
                                                
                                                <th class="td-sno">#</th>
                                                <th > Username</th>
                                                <th > Nomorktp</th>
                                                <th > Tempatlahir</th>
                                                <th > Tanggallahir</th>
                                                <th > Nohp</th>
                                                <th > Alamat</th>
                                                <th > Pendidikan</th>
                                                <th > Photo</th>
                                                <th > Fotoktp</th>
                                                <th > Sertifikat</th>
                                                <th > Ijasah</th>
                                                <th > Kk</th>
                                                <th > Npwp</th>
                                                <th > Instansi</th>
                                                
                                                <th class="td-btn"></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            
                                            <?php
                                            $counter = 0;
                                            
                                            foreach($records as $data){
                                            $rec_id = (!empty($data['ID']) ? urlencode($data['ID']) : null);
                                            $counter++;
                                            
                                            
                                            ?>
                                            <tr>
                                                
                                                <?php if($can_delete){ ?>
                                                
                                                <th class=" td-checkbox">
                                                    <label class="custom-control custom-checkbox custom-control-inline">
                                                        <input class="optioncheck custom-control-input" name="optioncheck[]" value="<?php echo $data['ID'] ?>" type="checkbox" />
                                                            <span class="custom-control-label"></span>
                                                        </label>
                                                    </th>
                                                    
                                                    <?php } ?>
                                                    
                                                    <th class="td-sno"><?php echo $counter; ?></th>
                                                    
                                                    
                                                    <td>
                                                        <a <?php if($can_edit){ ?> data-source='<?php echo json_encode_quote(Menu :: $INSTANSI); ?>' 
                                                            data-value="<?php echo $data['USERNAME']; ?>" 
                                                            data-pk="<?php echo $data['ID'] ?>" 
                                                            data-url="<?php print_link("data_ptt/editfield/" . urlencode($data['ID'])); ?>" 
                                                            data-name="USERNAME" 
                                                            data-title="Enter Username" 
                                                            data-placement="left" 
                                                            data-toggle="click" 
                                                            data-type="text" 
                                                            data-mode="popover" 
                                                            data-showbuttons="left" 
                                                            class="is-editable" <?php } ?>>
                                                            <?php echo $data['USERNAME']; ?>  
                                                        </a>
                                                    </td>
                                                    
                                                    
                                                    
                                                    
                                                    <td>
                                                        <a <?php if($can_edit){ ?> data-step="1" 
                                                            data-source='<?php echo json_encode_quote(Menu :: $INSTANSI); ?>' 
                                                            data-value="<?php echo $data['NOMORKTP']; ?>" 
                                                            data-pk="<?php echo $data['ID'] ?>" 
                                                            data-url="<?php print_link("data_ptt/editfield/" . urlencode($data['ID'])); ?>" 
                                                            data-name="NOMORKTP" 
                                                            data-title="Enter Nomorktp" 
                                                            data-placement="left" 
                                                            data-toggle="click" 
                                                            data-type="number" 
                                                            data-mode="popover" 
                                                            data-showbuttons="left" 
                                                            class="is-editable" <?php } ?>>
                                                            <?php echo $data['NOMORKTP']; ?>  
                                                        </a>
                                                    </td>
                                                    
                                                    
                                                    
                                                    
                                                    <td>
                                                        <a <?php if($can_edit){ ?> data-source='<?php echo json_encode_quote(Menu :: $INSTANSI); ?>' 
                                                            data-value="<?php echo $data['TEMPATLAHIR']; ?>" 
                                                            data-pk="<?php echo $data['ID'] ?>" 
                                                            data-url="<?php print_link("data_ptt/editfield/" . urlencode($data['ID'])); ?>" 
                                                            data-name="TEMPATLAHIR" 
                                                            data-title="Enter Tempat Lahir" 
                                                            data-placement="left" 
                                                            data-toggle="click" 
                                                            data-type="text" 
                                                            data-mode="popover" 
                                                            data-showbuttons="left" 
                                                            class="is-editable" <?php } ?>>
                                                            <?php echo $data['TEMPATLAHIR']; ?>  
                                                        </a>
                                                    </td>
                                                    
                                                    
                                                    
                                                    
                                                    <td>
                                                        <a <?php if($can_edit){ ?> data-source='<?php echo json_encode_quote(Menu :: $INSTANSI); ?>' 
                                                            data-value="<?php echo $data['TANGGALLAHIR']; ?>" 
                                                            data-pk="<?php echo $data['ID'] ?>" 
                                                            data-url="<?php print_link("data_ptt/editfield/" . urlencode($data['ID'])); ?>" 
                                                            data-name="TANGGALLAHIR" 
                                                            data-title="Enter Tanggal Lahir" 
                                                            data-placement="left" 
                                                            data-toggle="click" 
                                                            data-type="text" 
                                                            data-mode="popover" 
                                                            data-showbuttons="left" 
                                                            class="is-editable" <?php } ?>>
                                                            <?php echo $data['TANGGALLAHIR']; ?>  
                                                        </a>
                                                    </td>
                                                    
                                                    
                                                    
                                                    
                                                    <td>
                                                        <a <?php if($can_edit){ ?> data-step="1" 
                                                            data-source='<?php echo json_encode_quote(Menu :: $INSTANSI); ?>' 
                                                            data-value="<?php echo $data['NOHP']; ?>" 
                                                            data-pk="<?php echo $data['ID'] ?>" 
                                                            data-url="<?php print_link("data_ptt/editfield/" . urlencode($data['ID'])); ?>" 
                                                            data-name="NOHP" 
                                                            data-title="Enter Nohp" 
                                                            data-placement="left" 
                                                            data-toggle="click" 
                                                            data-type="number" 
                                                            data-mode="popover" 
                                                            data-showbuttons="left" 
                                                            class="is-editable" <?php } ?>>
                                                            <?php echo $data['NOHP']; ?>  
                                                        </a>
                                                    </td>
                                                    
                                                    
                                                    
                                                    
                                                    <td><div><?php echo $data['ALAMAT']; ?></div>
                                                    </td>
                                                    
                                                    
                                                    
                                                    
                                                    <td>
                                                        <a <?php if($can_edit){ ?> data-source='<?php echo json_encode_quote(Menu :: $INSTANSI); ?>' 
                                                            data-value="<?php echo $data['PENDIDIKAN']; ?>" 
                                                            data-pk="<?php echo $data['ID'] ?>" 
                                                            data-url="<?php print_link("data_ptt/editfield/" . urlencode($data['ID'])); ?>" 
                                                            data-name="PENDIDIKAN" 
                                                            data-title="Enter Pendidikan" 
                                                            data-placement="left" 
                                                            data-toggle="click" 
                                                            data-type="text" 
                                                            data-mode="popover" 
                                                            data-showbuttons="left" 
                                                            class="is-editable" <?php } ?>>
                                                            <?php echo $data['PENDIDIKAN']; ?>  
                                                        </a>
                                                    </td>
                                                    
                                                    
                                                    
                                                    
                                                    <td><?php Html :: page_img($data['PHOTO'],50,50,1); ?></td>
                                                    
                                                    
                                                    
                                                    
                                                    <td><?php Html :: page_img($data['FOTOKTP'],50,50,1); ?></td>
                                                    
                                                    
                                                    
                                                    
                                                    <td><?php Html :: page_img($data['SERTIFIKAT'],50,50,1); ?></td>
                                                    
                                                    
                                                    
                                                    
                                                    <td><?php Html :: page_img($data['IJASAH'],50,50,1); ?></td>
                                                    
                                                    
                                                    
                                                    
                                                    <td><?php Html :: page_img($data['KK'],50,50,1); ?></td>
                                                    
                                                    
                                                    
                                                    
                                                    <td><?php Html :: page_img($data['NPWP'],50,50,1); ?></td>
                                                    
                                                    
                                                    
                                                    
                                                    <td>
                                                        <a <?php if($can_edit){ ?> data-source='<?php print_link('api/json/data_ptt_INSTANSI_option_list'); ?>' 
                                                            data-value="<?php echo $data['INSTANSI']; ?>" 
                                                            data-pk="<?php echo $data['ID'] ?>" 
                                                            data-url="<?php print_link("data_ptt/editfield/" . urlencode($data['ID'])); ?>" 
                                                            data-name="INSTANSI" 
                                                            data-title="Silahkan Dipilih" 
                                                            data-placement="left" 
                                                            data-toggle="click" 
                                                            data-type="select" 
                                                            data-mode="popover" 
                                                            data-showbuttons="left" 
                                                            class="is-editable" <?php } ?>>
                                                            <?php echo $data['INSTANSI']; ?>  
                                                        </a>
                                                    </td>
                                                    
                                                    
                                                    
                                                    
                                                    <th class="td-btn">
                                                        
                                                        
                                                        <?php if($can_view){ ?>
                                                        
                                                        <a class="btn btn-sm btn-success has-tooltip" title="View Record" href="<?php print_link("data_ptt/view/$rec_id"); ?>">
                                                            <i class="fa fa-eye"></i> 
                                                        </a>
                                                        
                                                        <?php } ?>
                                                        
                                                        
                                                        <?php if($can_edit){ ?>
                                                        
                                                        <a class="btn btn-sm btn-info has-tooltip" title="Edit This Record" href="<?php print_link("data_ptt/edit/$rec_id"); ?>">
                                                            <i class="fa fa-edit"></i> 
                                                        </a>
                                                        
                                                        <?php } ?>
                                                        
                                                        
                                                        <?php if($can_delete){ ?>
                                                        
                                                        <a class="btn btn-sm btn-danger has-tooltip record-delete-btn" title="Delete this record" href="<?php print_link("data_ptt/delete/$rec_id/?csrf_token=$csrf_token"); ?>" data-prompt-msg="Yakin Ingin Dihapus" data-display-style="modal">
                                                            <i class="fa fa-times"></i>
                                                            
                                                        </a>
                                                        
                                                        <a target='_blank' href='<?php print_link("cetaksemua.php") ?>?NOMORKTP=<?php echo $data['NOMORKTP']?>'>cetak<i class="fa fa-edit"></i></a>
                                                        
                                                        <?php } ?>
                                                        
                                                        
                                                    </th>
                                                </tr>
                                                <?php 
                                                }
                                                ?>
                                                
                                                
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                    <?php
                                    if( $show_footer == true ){
                                    ?>
                                    <div class="">
                                        <div class="row">   
                                            <div class="col-sm-4">  
                                                <div class="py-2">  
                                                    
                                                    <?php if($can_delete){ ?>
                                                    
                                                    <button data-prompt-msg="Are you sure you want to delete these records?" data-display-style="modal" data-url="<?php print_link("data_ptt/delete/{sel_ids}/?csrf_token=$csrf_token"); ?>" class="btn btn-sm btn-danger btn-delete-selected d-none">
                                                        <i class="fa fa-times"></i> Delete Selected
                                                    </button>
                                                    
                                                    <?php } ?>
                                                    
                                                    
                                                    <button class="btn btn-sm btn-primary export-btn"><i class="fa fa-save"></i> </button>
                                                    
                                                    
                                                    <?php Html :: import_form('data_ptt/import_data' , "", 'CSV , JSON'); ?>
                                                    
                                                </div>
                                            </div>
                                            <div class="col">   
                                                
                                                <?php
                                                if( $show_pagination == true ){
                                                $pager = new Pagination($total_records,$record_count);
                                                $pager->page_name='data_ptt';
                                                $pager->show_page_count=true;
                                                $pager->show_record_count=true;
                                                $pager->show_page_limit=true;
                                                $pager->show_page_number_list=true;
                                                $pager->pager_link_range=5;
                                                
                                                $pager->render();
                                                }
                                                ?>
                                                
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                    }
                                    }
                                    else{
                                    ?>
                                    <div class="text-muted animated bounce  p-3">
                                        <h4><i class="fa fa-ban"></i> </h4>
                                    </div>
                                    <?php
                                    }
                                    ?>
                                    
                                </div>
                            </div>
                            
                        </div>
                        
                    </div>
                </div>
            </div>
            
        </section>
        