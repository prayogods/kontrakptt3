
<?php
$comp_model = new SharedController;

$csrf_token = Csrf :: $token;

$data = $this->view_data;

//$rec_id = $data['__tableprimarykey'];
$page_id = Router :: $page_id;

$show_header = $this->show_header;
$view_title = $this->view_title;
$redirect_to = $this->redirect_to;

?>

<section class="page">
    
    <?php
    if( $show_header == true ){
    ?>
    
    <div  class="bg-light p-3 mb-3">
        <div class="container">
            
            <div class="row ">
                
                <div class="col-12 ">
                    <h3 class="record-title">Edit  Data Ptt</h3>
                    
                </div>
                
            </div>
        </div>
    </div>
    
    <?php
    }
    ?>
    
    <div  class="">
        <div class="container">
            
            <div class="row ">
                
                <div class="col-md-7 comp-grid">
                    
                    <?php $this :: display_page_errors(); ?>
                    
                    <div  class=" animated fadeIn">
                        <form novalidate  id="" role="form" enctype="multipart/form-data"  class="form form-horizontal needs-validation" action="<?php print_link("data_ptt/edit/$page_id/?csrf_token=$csrf_token"); ?>" method="post">
                            <div>
                                
                                
                                <div class="form-group ">
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <label class="control-label" for="USERNAME">Username <span class="text-danger">*</span></label>
                                        </div>
                                        <div class="col-sm-8">
                                            <div class="">
                                                <input id="ctrl-USERNAME"  value="<?php  echo $data['USERNAME']; ?>" type="text" placeholder="Enter Username"  readonly required="" name="USERNAME"  class="form-control " />
                                                    
                                                    
                                                    
                                                </div>
                                                
                                                
                                            </div>
                                        </div>
                                    </div>
                                    
                                    
                                    
                                    
                                    <div class="form-group ">
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <label class="control-label" for="NOMORKTP">Nomorktp <span class="text-danger">*</span></label>
                                            </div>
                                            <div class="col-sm-8">
                                                <div class="">
                                                    <input id="ctrl-NOMORKTP"  value="<?php  echo $data['NOMORKTP']; ?>" type="number" placeholder="Enter Nomorktp" step="1"  required="" name="NOMORKTP"  class="form-control " />
                                                        
                                                        
                                                        
                                                    </div>
                                                    
                                                    
                                                </div>
                                            </div>
                                        </div>
                                        
                                        
                                        
                                        
                                        <div class="form-group ">
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <label class="control-label" for="TEMPATLAHIR">Tempat Lahir <span class="text-danger">*</span></label>
                                                </div>
                                                <div class="col-sm-8">
                                                    <div class="">
                                                        <input id="ctrl-TEMPATLAHIR"  value="<?php  echo $data['TEMPATLAHIR']; ?>" type="text" placeholder="Enter Tempat Lahir"  required="" name="TEMPATLAHIR"  class="form-control " />
                                                            
                                                            
                                                            
                                                        </div>
                                                        
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            
                                            
                                            
                                            <div class="form-group ">
                                                <div class="row">
                                                    <div class="col-sm-4">
                                                        <label class="control-label" for="TANGGALLAHIR">Tanggal Lahir <span class="text-danger">*</span></label>
                                                    </div>
                                                    <div class="col-sm-8">
                                                        <div class="input-group">
                                                            <input id="ctrl-TANGGALLAHIR" class="form-control datepicker  datepicker"  required="" value="<?php  echo $data['TANGGALLAHIR']; ?>" type="datetime" name="TANGGALLAHIR" placeholder="Enter Tanggal Lahir" data-enable-time="false" data-min-date="" data-max-date="" data-date-format="Y-m-d" data-alt-format="F j, Y" data-inline="false" data-no-calendar="false" data-mode="single" />
                                                                
                                                                
                                                                <div class="input-group-append">
                                                                    <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                                                </div>
                                                                
                                                            </div>
                                                            
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                
                                                
                                                
                                                <div class="form-group ">
                                                    <div class="row">
                                                        <div class="col-sm-4">
                                                            <label class="control-label" for="NOHP">Nohp <span class="text-danger">*</span></label>
                                                        </div>
                                                        <div class="col-sm-8">
                                                            <div class="">
                                                                <input id="ctrl-NOHP"  value="<?php  echo $data['NOHP']; ?>" type="number" placeholder="Enter Nohp" step="1"  required="" name="NOHP"  class="form-control " />
                                                                    
                                                                    
                                                                    
                                                                </div>
                                                                
                                                                
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                    
                                                    
                                                    
                                                    <div class="form-group ">
                                                        <div class="row">
                                                            <div class="col-sm-4">
                                                                <label class="control-label" for="ALAMAT">Alamat <span class="text-danger">*</span></label>
                                                            </div>
                                                            <div class="col-sm-8">
                                                                <div class="">
                                                                    
                                                                    <textarea placeholder="Enter Alamat" id="ctrl-ALAMAT"  required="" rows="" name="ALAMAT" class="htmleditor form-control"><?php  echo $data['ALAMAT']; ?></textarea>
                                                                    <!--<div class="invalid-feedback animated bounceIn text-center">Please enter text</div>-->
                                                                    
                                                                    
                                                                </div>
                                                                
                                                                
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                    
                                                    
                                                    
                                                    <div class="form-group ">
                                                        <div class="row">
                                                            <div class="col-sm-4">
                                                                <label class="control-label" for="PENDIDIKAN">Pendidikan <span class="text-danger">*</span></label>
                                                            </div>
                                                            <div class="col-sm-8">
                                                                <div class="">
                                                                    <input id="ctrl-PENDIDIKAN"  value="<?php  echo $data['PENDIDIKAN']; ?>" type="text" placeholder="Enter Pendidikan"  required="" name="PENDIDIKAN"  class="form-control " />
                                                                        
                                                                        
                                                                        
                                                                    </div>
                                                                    
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                        
                                                        
                                                        
                                                        
                                                        <div class="form-group ">
                                                            <div class="row">
                                                                <div class="col-sm-4">
                                                                    <label class="control-label" for="PHOTO">Photo <span class="text-danger">*</span></label>
                                                                </div>
                                                                <div class="col-sm-8">
                                                                    <div class="">
                                                                        
                                                                        <div class="dropzone required" id="PHOTO_upload" input="#ctrl-PHOTO" fieldname="PHOTO"    data-multiple="false"    btntext="Browse" extensions=".jpg,.png,.gif,.jpeg" filesize="3" maximum="1">
                                                                            <input name="PHOTO" id="ctrl-PHOTO" required="" class="dropzone-input form-control" value="<?php  echo $data['PHOTO']; ?>" type="text"  />
                                                                                <!--<div class="invalid-feedback animated bounceIn text-center">Please a choose file</div>-->
                                                                                <div class="dz-file-limit animated bounceIn text-center text-danger"></div>
                                                                            </div>
                                                                            
                                                                            
                                                                        </div>
                                                                        
                                                                        <?php Html :: uploaded_files_list($data['PHOTO'], '#ctrl-PHOTO'); ?>
                                                                        
                                                                        
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            
                                                            
                                                            
                                                            
                                                            <div class="form-group ">
                                                                <div class="row">
                                                                    <div class="col-sm-4">
                                                                        <label class="control-label" for="FOTOKTP">Fotoktp <span class="text-danger">*</span></label>
                                                                    </div>
                                                                    <div class="col-sm-8">
                                                                        <div class="">
                                                                            
                                                                            <div class="dropzone required" id="FOTOKTP_upload" input="#ctrl-FOTOKTP" fieldname="FOTOKTP"    data-multiple="false"    btntext="Browse" extensions=".jpg,.png,.gif,.jpeg" filesize="3" maximum="1">
                                                                                <input name="FOTOKTP" id="ctrl-FOTOKTP" required="" class="dropzone-input form-control" value="<?php  echo $data['FOTOKTP']; ?>" type="text"  />
                                                                                    <!--<div class="invalid-feedback animated bounceIn text-center">Please a choose file</div>-->
                                                                                    <div class="dz-file-limit animated bounceIn text-center text-danger"></div>
                                                                                </div>
                                                                                
                                                                                
                                                                            </div>
                                                                            
                                                                            <?php Html :: uploaded_files_list($data['FOTOKTP'], '#ctrl-FOTOKTP'); ?>
                                                                            
                                                                            
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                
                                                                
                                                                
                                                                
                                                                <div class="form-group ">
                                                                    <div class="row">
                                                                        <div class="col-sm-4">
                                                                            <label class="control-label" for="SERTIFIKAT">Sertifikat </label>
                                                                        </div>
                                                                        <div class="col-sm-8">
                                                                            <div class="">
                                                                                
                                                                                <div class="dropzone " id="SERTIFIKAT_upload" input="#ctrl-SERTIFIKAT" fieldname="SERTIFIKAT"    data-multiple="false"    btntext="Browse" extensions=".jpg,.png,.gif,.jpeg" filesize="3" maximum="1">
                                                                                    <input name="SERTIFIKAT" id="ctrl-SERTIFIKAT" class="dropzone-input form-control" value="<?php  echo $data['SERTIFIKAT']; ?>" type="text"  />
                                                                                        <!--<div class="invalid-feedback animated bounceIn text-center">Please a choose file</div>-->
                                                                                        <div class="dz-file-limit animated bounceIn text-center text-danger"></div>
                                                                                    </div>
                                                                                    
                                                                                    
                                                                                </div>
                                                                                
                                                                                <?php Html :: uploaded_files_list($data['SERTIFIKAT'], '#ctrl-SERTIFIKAT'); ?>
                                                                                
                                                                                
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    
                                                                    
                                                                    
                                                                    
                                                                    <div class="form-group ">
                                                                        <div class="row">
                                                                            <div class="col-sm-4">
                                                                                <label class="control-label" for="IJASAH">Ijasah </label>
                                                                            </div>
                                                                            <div class="col-sm-8">
                                                                                <div class="">
                                                                                    
                                                                                    <div class="dropzone " id="IJASAH_upload" input="#ctrl-IJASAH" fieldname="IJASAH"    data-multiple="false"    btntext="Browse" extensions=".jpg,.png,.gif,.jpeg" filesize="3" maximum="1">
                                                                                        <input name="IJASAH" id="ctrl-IJASAH" class="dropzone-input form-control" value="<?php  echo $data['IJASAH']; ?>" type="text"  />
                                                                                            <!--<div class="invalid-feedback animated bounceIn text-center">Please a choose file</div>-->
                                                                                            <div class="dz-file-limit animated bounceIn text-center text-danger"></div>
                                                                                        </div>
                                                                                        
                                                                                        
                                                                                    </div>
                                                                                    
                                                                                    <?php Html :: uploaded_files_list($data['IJASAH'], '#ctrl-IJASAH'); ?>
                                                                                    
                                                                                    
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        
                                                                        
                                                                        
                                                                        
                                                                        <div class="form-group ">
                                                                            <div class="row">
                                                                                <div class="col-sm-4">
                                                                                    <label class="control-label" for="KK">Kk </label>
                                                                                </div>
                                                                                <div class="col-sm-8">
                                                                                    <div class="">
                                                                                        
                                                                                        <div class="dropzone " id="KK_upload" input="#ctrl-KK" fieldname="KK"    data-multiple="false"    btntext="Browse" extensions=".jpg,.png,.gif,.jpeg" filesize="3" maximum="1">
                                                                                            <input name="KK" id="ctrl-KK" class="dropzone-input form-control" value="<?php  echo $data['KK']; ?>" type="text"  />
                                                                                                <!--<div class="invalid-feedback animated bounceIn text-center">Please a choose file</div>-->
                                                                                                <div class="dz-file-limit animated bounceIn text-center text-danger"></div>
                                                                                            </div>
                                                                                            
                                                                                            
                                                                                        </div>
                                                                                        
                                                                                        <?php Html :: uploaded_files_list($data['KK'], '#ctrl-KK'); ?>
                                                                                        
                                                                                        
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            
                                                                            
                                                                            
                                                                            
                                                                            <div class="form-group ">
                                                                                <div class="row">
                                                                                    <div class="col-sm-4">
                                                                                        <label class="control-label" for="NPWP">Npwp </label>
                                                                                    </div>
                                                                                    <div class="col-sm-8">
                                                                                        <div class="">
                                                                                            
                                                                                            <div class="dropzone " id="NPWP_upload" input="#ctrl-NPWP" fieldname="NPWP"    data-multiple="false"    btntext="Browse" extensions=".jpg,.png,.gif,.jpeg" filesize="3" maximum="1">
                                                                                                <input name="NPWP" id="ctrl-NPWP" class="dropzone-input form-control" value="<?php  echo $data['NPWP']; ?>" type="text"  />
                                                                                                    <!--<div class="invalid-feedback animated bounceIn text-center">Please a choose file</div>-->
                                                                                                    <div class="dz-file-limit animated bounceIn text-center text-danger"></div>
                                                                                                </div>
                                                                                                
                                                                                                
                                                                                            </div>
                                                                                            
                                                                                            <?php Html :: uploaded_files_list($data['NPWP'], '#ctrl-NPWP'); ?>
                                                                                            
                                                                                            
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                
                                                                                
                                                                                
                                                                                
                                                                                <div class="form-group ">
                                                                                    <div class="row">
                                                                                        <div class="col-sm-4">
                                                                                            <label class="control-label" for="nomorkontrak">Nomorkontrak </label>
                                                                                        </div>
                                                                                        <div class="col-sm-8">
                                                                                            <div class="">
                                                                                                <input id="ctrl-nomorkontrak"  value="<?php  echo $data['nomorkontrak']; ?>" type="text" placeholder="Enter Nomorkontrak"  name="nomorkontrak"  class="form-control " />
                                                                                                    
                                                                                                    
                                                                                                    
                                                                                                </div>
                                                                                                
                                                                                                
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    
                                                                                    
                                                                                    
                                                                                    
                                                                                    <div class="form-group ">
                                                                                        <div class="row">
                                                                                            <div class="col-sm-4">
                                                                                                <label class="control-label" for="kontraklama">Kontraklama </label>
                                                                                            </div>
                                                                                            <div class="col-sm-8">
                                                                                                <div class="">
                                                                                                    <input id="ctrl-kontraklama"  value="<?php  echo $data['kontraklama']; ?>" type="text" placeholder="Enter Kontraklama"  name="kontraklama"  class="form-control " />
                                                                                                        
                                                                                                        
                                                                                                        
                                                                                                    </div>
                                                                                                    
                                                                                                    
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        
                                                                                        
                                                                                        
                                                                                        
                                                                                        <div class="form-group ">
                                                                                            <div class="row">
                                                                                                <div class="col-sm-4">
                                                                                                    <label class="control-label" for="INSTANSI">Instansi <span class="text-danger">*</span></label>
                                                                                                </div>
                                                                                                <div class="col-sm-8">
                                                                                                    <div class="">
                                                                                                        
                                                                                                        <select required=""  id="ctrl-INSTANSI" name="INSTANSI"  placeholder="Silahkan Dipilih"    class="custom-select" >
                                                                                                            
                                                                                                            <option value="">Silahkan Dipilih</option>
                                                                                                            
                                                                                                            
                                                                                                            <?php
                                                                                                            $rec = $data['INSTANSI'];
                                                                                                            $INSTANSI_options = $comp_model -> data_ptt_INSTANSI_option_list();
                                                                                                            if(!empty($INSTANSI_options)){
                                                                                                            foreach($INSTANSI_options as $arr){
                                                                                                            $val = array_values($arr);
                                                                                                            $selected = ( $val[0] == $rec ? 'selected' : null );
                                                                                                            ?>
                                                                                                            <option 
                                                                                                                <?php echo $selected; ?> value="<?php echo $val[0]; ?>"><?php echo (!empty($val[1]) ? $val[1] : $val[0]); ?>
                                                                                                            </option>
                                                                                                            <?php
                                                                                                            }
                                                                                                            }
                                                                                                            ?>
                                                                                                            
                                                                                                        </select>
                                                                                                        
                                                                                                        
                                                                                                    </div>
                                                                                                    
                                                                                                    
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        
                                                                                        
                                                                                        
                                                                                        
                                                                                        <div class="form-group ">
                                                                                            <div class="row">
                                                                                                <div class="col-sm-4">
                                                                                                    <label class="control-label" for="MASAKERJA">Masakerja </label>
                                                                                                </div>
                                                                                                <div class="col-sm-8">
                                                                                                    <div class="">
                                                                                                        <input id="ctrl-MASAKERJA"  value="<?php  echo $data['MASAKERJA']; ?>" type="text" placeholder="1 Tahun"  name="MASAKERJA"  class="form-control " />
                                                                                                            
                                                                                                            
                                                                                                            
                                                                                                        </div>
                                                                                                        
                                                                                                        
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                            
                                                                                            
                                                                                            
                                                                                            
                                                                                        </div>
                                                                                        <div class="form-ajax-status"></div>
                                                                                        <div class="form-group text-center">
                                                                                            <button class="btn btn-primary" type="submit">
                                                                                                Ubah Data
                                                                                                <i class="fa fa-send"></i>
                                                                                            </button>
                                                                                        </div>
                                                                                    </form>
                                                                                </div>
                                                                                
                                                                            </div>
                                                                            
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                
                                                            </section>
                                                            