
<?php 
//check if current user role is allowed access to the pages
$can_add = PageAccessManager::is_allowed('pejabat/add');
$can_edit = PageAccessManager::is_allowed('pejabat/edit');
$can_view = PageAccessManager::is_allowed('pejabat/view');
$can_delete = PageAccessManager::is_allowed('pejabat/delete');
?>

<?php

$comp_model = new SharedController;

$csrf_token = Csrf :: $token;

//Page Data From Controller
$view_data = $this->view_data;

$records = $view_data->records;
$record_count = $view_data->record_count;
$total_records = $view_data->total_records;

$field_name = Router :: $field_name;
$field_value = Router :: $field_value;

$view_title = $this->view_title;
$show_header = $this->show_header;
$show_footer = $this->show_footer;
$show_pagination = $this->show_pagination;


?>

<section class="page">
    
    <?php
    if( $show_header == true ){
    ?>
    
    <div  class="bg-light p-3 mb-3">
        <div class="container-fluid">
            
            <div class="row ">
                
                <div class="col-sm-4 ">
                    <h3 class="record-title">Daftar Pejabat</h3>
                    
                </div>
                
                <div class="col-sm-3 ">
                    
                    <?php if($can_add){ ?>
                    
                    <a  class="btn btn btn-primary my-1" href="<?php print_link("pejabat/add") ?>">
                        <i class="fa fa-plus"></i>                              
                        Add New Pejabat 
                    </a>
                    
                    <?php } ?>
                    
                </div>
                
                <div class="col-sm-5 ">
                    
                </div>
                
                <div class="col-md-12 comp-grid">
                    <div class="">
                        <?php
                        if(!empty($field_name) || !empty($_GET['search'])){
                        ?>
                        <hr class="sm d-block d-sm-none" />
                        <nav class="page-header-breadcrumbs mt-2" aria-label="breadcrumb">
                            <ul class="breadcrumb m-0 p-1">
                                <?php
                                if(!empty($field_name)){
                                ?>
                                <li class="breadcrumb-item"><a class="text-capitalize" href="<?php print_link('pejabat') ?>"><?php echo $field_name ?></a></li>
                                <li  class="breadcrumb-item active text-capitalize"><?php echo urldecode($field_value) ?></li>
                                <?php 
                                }   
                                ?>
                                
                                <?php
                                if(!empty($_GET['search'])){
                                ?>
                                <li class="breadcrumb-item">
                                    <a class="text-capitalize" href="<?php print_link('pejabat') ?>">Search</a>
                                </li>
                                <li  class="breadcrumb-item active text-capitalize"> <strong><?php echo get_value('search'); ?></strong></li>
                                <?php
                                }
                                ?>
                                
                            </ul>
                        </nav>  
                        <?php
                        }
                        ?>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
    
    <?php
    }
    ?>
    
    <div  class="">
        <div class="container-fluid">
            
            <div class="row ">
                
                <div class="col-md-12 comp-grid">
                    
                    <?php $this :: display_page_errors(); ?>
                    
                    <div  class=" animated fadeInDown">
                        <div id="pejabat-list-records">
                            
                            <?php
                            if(!empty($records)){
                            ?>
                            <div class="page-records table-responsive">
                                <table class="table  table-striped table-sm">
                                    <thead class="table-header bg-light">
                                        <tr>
                                            
                                            <?php if($can_delete){ ?>
                                            
                                            <th class="td-sno td-checkbox">
                                                <label class="custom-control custom-checkbox custom-control-inline">
                                                    <input class="toggle-check-all custom-control-input" type="checkbox" />
                                                    <span class="custom-control-label"></span>
                                                </label>
                                            </th>
                                            
                                            <?php } ?>
                                            
                                            <th class="td-sno">#</th>
                                            <th > Id</th>
                                            <th > Ppk</th>
                                            <th > Kpa</th>
                                            <th > File</th>
                                            
                                            <th class="td-btn"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        
                                        <?php
                                        $counter = 0;
                                        
                                        foreach($records as $data){
                                        $rec_id = (!empty($data['ID']) ? urlencode($data['ID']) : null);
                                        $counter++;
                                        
                                        
                                        ?>
                                        <tr>
                                            
                                            <?php if($can_delete){ ?>
                                            
                                            <th class=" td-checkbox">
                                                <label class="custom-control custom-checkbox custom-control-inline">
                                                    <input class="optioncheck custom-control-input" name="optioncheck[]" value="<?php echo $data['ID'] ?>" type="checkbox" />
                                                        <span class="custom-control-label"></span>
                                                    </label>
                                                </th>
                                                
                                                <?php } ?>
                                                
                                                <th class="td-sno"><?php echo $counter; ?></th>
                                                
                                                
                                                <td><a href="<?php print_link("pejabat/view/$data[ID]") ?>"><?php echo $data['ID']; ?></a></td>
                                                
                                                
                                                
                                                
                                                <td>
                                                    <a <?php if($can_edit){ ?> data-source='<?php echo json_encode_quote(Menu :: $INSTANSI); ?>' 
                                                        data-value="<?php echo $data['PPK']; ?>" 
                                                        data-pk="<?php echo $data['ID'] ?>" 
                                                        data-url="<?php print_link("pejabat/editfield/" . urlencode($data['ID'])); ?>" 
                                                        data-name="PPK" 
                                                        data-title="Enter Ppk" 
                                                        data-placement="left" 
                                                        data-toggle="click" 
                                                        data-type="text" 
                                                        data-mode="popover" 
                                                        data-showbuttons="left" 
                                                        class="is-editable" <?php } ?>>
                                                        <?php echo $data['PPK']; ?>  
                                                    </a>
                                                </td>
                                                
                                                
                                                
                                                
                                                <td>
                                                    <a <?php if($can_edit){ ?> data-source='<?php echo json_encode_quote(Menu :: $INSTANSI); ?>' 
                                                        data-value="<?php echo $data['KPA']; ?>" 
                                                        data-pk="<?php echo $data['ID'] ?>" 
                                                        data-url="<?php print_link("pejabat/editfield/" . urlencode($data['ID'])); ?>" 
                                                        data-name="KPA" 
                                                        data-title="Enter Kpa" 
                                                        data-placement="left" 
                                                        data-toggle="click" 
                                                        data-type="text" 
                                                        data-mode="popover" 
                                                        data-showbuttons="left" 
                                                        class="is-editable" <?php } ?>>
                                                        <?php echo $data['KPA']; ?>  
                                                    </a>
                                                </td>
                                                
                                                
                                                
                                                
                                                <td>
                                                    <a <?php if($can_edit){ ?> data-source='<?php echo json_encode_quote(Menu :: $INSTANSI); ?>' 
                                                        data-value="<?php echo $data['FILE']; ?>" 
                                                        data-pk="<?php echo $data['ID'] ?>" 
                                                        data-url="<?php print_link("pejabat/editfield/" . urlencode($data['ID'])); ?>" 
                                                        data-name="FILE" 
                                                        data-title="Browse..." 
                                                        data-placement="left" 
                                                        data-toggle="click" 
                                                        data-type="text" 
                                                        data-mode="popover" 
                                                        data-showbuttons="left" 
                                                        class="is-editable" <?php } ?>>
                                                        <?php echo $data['FILE']; ?>  
                                                    </a>
                                                </td>
                                                
                                                
                                                
                                                
                                                <th class="td-btn">
                                                    
                                                    
                                                    <?php if($can_view){ ?>
                                                    
                                                    <a class="btn btn-sm btn-success has-tooltip" title="View Record" href="<?php print_link("pejabat/view/$rec_id"); ?>">
                                                        <i class="fa fa-eye"></i> 
                                                    </a>
                                                    
                                                    <?php } ?>
                                                    
                                                    
                                                    <?php if($can_edit){ ?>
                                                    
                                                    <a class="btn btn-sm btn-info has-tooltip" title="Edit This Record" href="<?php print_link("pejabat/edit/$rec_id"); ?>">
                                                        <i class="fa fa-edit"></i> 
                                                    </a>
                                                    
                                                    <?php } ?>
                                                    
                                                    
                                                    <?php if($can_delete){ ?>
                                                    
                                                    <a class="btn btn-sm btn-danger has-tooltip record-delete-btn" title="Delete this record" href="<?php print_link("pejabat/delete/$rec_id/?csrf_token=$csrf_token"); ?>" data-prompt-msg="Yakin Ingin Dihapus?" data-display-style="none">
                                                        <i class="fa fa-times"></i>
                                                        
                                                    </a>
                                                    
                                                    <?php } ?>
                                                    
                                                    
                                                </th>
                                            </tr>
                                            <?php 
                                            }
                                            ?>
                                            
                                            
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td></td><td></td><td></td><td></td><td></td><td></td><td></td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                                <?php
                                if( $show_footer == true ){
                                ?>
                                <div class="">
                                    <div class="row">   
                                        <div class="col-sm-4">  
                                            <div class="py-2">  
                                                
                                                <?php if($can_delete){ ?>
                                                
                                                <button data-prompt-msg="Are you sure you want to delete these records?" data-display-style="none" data-url="<?php print_link("pejabat/delete/{sel_ids}/?csrf_token=$csrf_token"); ?>" class="btn btn-sm btn-danger btn-delete-selected d-none">
                                                    <i class="fa fa-times"></i> Delete Selected
                                                </button>
                                                
                                                <?php } ?>
                                                
                                                
                                                <button class="btn btn-sm btn-primary export-btn"><i class="fa fa-save"></i> </button>
                                                
                                                
                                                <?php Html :: import_form('pejabat/import_data' , "", 'CSV , JSON'); ?>
                                                
                                            </div>
                                        </div>
                                        <div class="col">   
                                            
                                            <?php
                                            if( $show_pagination == true ){
                                            $pager = new Pagination($total_records,$record_count);
                                            $pager->page_name='pejabat';
                                            $pager->show_page_count=true;
                                            $pager->show_record_count=true;
                                            $pager->show_page_limit=true;
                                            $pager->show_page_number_list=true;
                                            $pager->pager_link_range=5;
                                            
                                            $pager->render();
                                            }
                                            ?>
                                            
                                        </div>
                                    </div>
                                </div>
                                <?php
                                }
                                }
                                else{
                                ?>
                                <div class="text-muted animated bounce  p-3">
                                    <h4><i class="fa fa-ban"></i> </h4>
                                </div>
                                <?php
                                }
                                ?>
                                
                            </div>
                        </div>
                        
                    </div>
                    
                </div>
            </div>
        </div>
        
    </section>
    