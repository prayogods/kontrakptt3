
<?php 
//check if current user role is allowed access to the pages
$can_add = PageAccessManager::is_allowed('akun/add');
$can_edit = PageAccessManager::is_allowed('akun/edit');
$can_view = PageAccessManager::is_allowed('akun/view');
$can_delete = PageAccessManager::is_allowed('akun/delete');
?>

<?php
$comp_model = new SharedController;

$csrf_token = Csrf :: $token;

//Page Data Information from Controller
$data = $this->view_data;

//$rec_id = $data['__tableprimarykey'];
$page_id = Router::$page_id; //Page id from url

$view_title = $this->view_title;

$show_header = $this->show_header;
$show_edit_btn = $this->show_edit_btn;
$show_delete_btn = $this->show_delete_btn;
$show_export_btn = $this->show_export_btn;

?>

<section class="page">
    
    <?php
    if( $show_header == true ){
    ?>
    
    <div  class="bg-light p-3 mb-3">
        <div class="container">
            
            <div class="row ">
                
                <div class="col-12 ">
                    <h3 class="record-title">My Account</h3>
                    
                </div>
                
            </div>
        </div>
    </div>
    
    <?php
    }
    ?>
    
    <div  class="">
        <div class="container">
            
            <div class="row ">
                
                <div class="col-md-12 comp-grid">
                    
                    <?php $this :: display_page_errors(); ?>
                    
                    <div  class=" animated fadeIn">
                        <div class="profile-bg mb-2">
                            <div class="profile">
                                <div class="avatar">
                                    <?php 
                                    if(!empty($data['PHOTO'])){
                                    Html::page_img($data['PHOTO'],100,100); 
                                    }
                                    ?>
                                </div>
                                <h1 class="title mt-4"><?php echo $data['USERNAME']; ?></h1>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="mb-3 card p-2">
                                    <ul class="nav nav-pills flex-column text-left">
                                        <li class="nav-item"><a data-toggle="tab" href="#AccountPageView" class="nav-link active"><i class="fa fa-user"></i> Account Detail</a></li>
                                        <li class="nav-item"><a data-toggle="tab" href="#AccountPageEdit" class="nav-link"><i class="fa fa-edit"></i> Edit Account</a></li>
                                        <li class="nav-item"><a data-toggle="tab" href="#AccountPageChangeEmail" class="nav-link"><i class="fa fa-user"></i> Change Email</a></li>
                                        <li class="nav-item"><a data-toggle="tab" href="#AccountPageChangePassword" class="nav-link"><i class="fa fa-key"></i> Reset Password</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-sm-9">
                                <div class="mb-3">
                                    <div class="tab-content">
                                        <div class="tab-pane show active fade" id="AccountPageView" role="tabpanel">
                                            <table class="table table-hover table-borderless table-striped">
                                                <tbody>
                                                    
                                                    <tr>
                                                        <th class="title"> Id :</th>
                                                        <td class="value"> <?php echo $data['ID']; ?> </td>
                                                    </tr>
                                                    
                                                    
                                                    <tr>
                                                        <th class="title"> Username :</th>
                                                        <td class="value"> <?php echo $data['USERNAME']; ?> </td>
                                                    </tr>
                                                    
                                                    
                                                    <tr>
                                                        <th class="title"> Email :</th>
                                                        <td class="value"> <?php echo $data['EMAIL']; ?> </td>
                                                    </tr>
                                                    
                                                    
                                                    <tr>
                                                        <th class="title"> Role :</th>
                                                        <td class="value"> <?php echo $data['ROLE']; ?> </td>
                                                    </tr>
                                                    
                                                    
                                                    <tr>
                                                        <th class="title"> Nomorktp :</th>
                                                        <td class="value"> <?php echo $data['NOMORKTP']; ?> </td>
                                                    </tr>
                                                    
                                                    
                                                    <tr>
                                                        <th class="title"> Nomorhp :</th>
                                                        <td class="value"> <?php echo $data['NOMORHP']; ?> </td>
                                                    </tr>
                                                    
                                                    
                                                </tbody>    
                                            </table>
                                        </div>
                                        <div class="tab-pane fade" id="AccountPageEdit" role="tabpanel">
                                            <div class=" reset-grids">
                                                <?php  $this->render_page("account/edit"); ?>
                                            </div>
                                        </div>
                                        <div class="tab-pane  fade" id="AccountPageChangeEmail" role="tabpanel">
                                            <div class=" reset-grids">
                                                <?php  $this->render_page("account/change_email"); ?>
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="AccountPageChangePassword" role="tabpanel">
                                            <div class=" reset-grids">
                                                <?php  $this->render_page("passwordmanager"); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
                
            </div>
        </div>
    </div>
    
</section>
