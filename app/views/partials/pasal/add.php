
<?php
$comp_model = new SharedController;

$csrf_token = Csrf :: $token;

$show_header = $this->show_header;
$view_title = $this->view_title;
$redirect_to = $this->redirect_to;

?>

<section class="page">
    
    <?php
    if( $show_header == true ){
    ?>
    
    <div  class="bg-light p-3 mb-3">
        <div class="container">
            
            <div class="row ">
                
                <div class="col-12 ">
                    <h3 class="record-title">Add New Pasal</h3>
                    
                </div>
                
            </div>
        </div>
    </div>
    
    <?php
    }
    ?>
    
    <div  class="">
        <div class="container">
            
            <div class="row ">
                
                <div class="col-md-7 comp-grid">
                    
                    <?php $this :: display_page_errors(); ?>
                    
                    <div  class=" animated fadeIn">
                        <form id="pasal-add-form" role="form" novalidate enctype="multipart/form-data" class="form form-horizontal needs-validation" action="<?php print_link("pasal/add?csrf_token=$csrf_token") ?>" method="post">
                            <div>
                                
                                
                                <div class="form-group ">
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <label class="control-label" for="INSTANSI">Instansi <span class="text-danger">*</span></label>
                                        </div>
                                        <div class="col-sm-8">
                                            <div class="">
                                                
                                                <select required=""  id="ctrl-INSTANSI" name="INSTANSI"  placeholder="Select a value ..."    class="custom-select" >
                                                    <option value="">Select a value ...</option>
                                                    
                                                    <?php 
                                                    $INSTANSI_options = $comp_model -> pasal_INSTANSI_option_list();
                                                    if(!empty($INSTANSI_options)){
                                                    foreach($INSTANSI_options as $arr){
                                                    $val = array_values($arr);
                                                    $selected = $this->set_field_selected('INSTANSI',$val[0], '');
                                                    ?>
                                                    <option <?php echo $selected; ?> value="<?php echo $val[0]; ?>">
                                                        <?php echo (!empty($val[1]) ? $val[1] : $val[0]); ?>
                                                    </option>
                                                    <?php
                                                    }
                                                    }
                                                    ?>
                                                    
                                                </select>
                                                
                                                
                                            </div>
                                            
                                            
                                        </div>
                                    </div>
                                </div>
                                
                                
                                
                                <div class="row">
                                    
                                    <div class="form-group col-md-12">
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <label class="control-label" for="JUDUL">Judul Kontrak </label>
                                            </div>
                                            <div class="col-sm-8">
                                                <div class="">
                                                    
                                                    <textarea placeholder="Enter Judul Kontrak" id="ctrl-JUDUL"  rows="" name="JUDUL" class="htmleditor form-control"><?php  echo $this->set_field_value('JUDUL',''); ?></textarea>
                                                    <!--<div class="invalid-feedback animated bounceIn text-center">Please enter text</div>-->
                                                    
                                                    
                                                </div>
                                                
                                                
                                            </div>
                                        </div>
                                    </div>
                                    
                                    
                                    
                                </div>
                                
                                <div class="form-group ">
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <label class="control-label" for="PEMBUKA">Pembuka </label>
                                        </div>
                                        <div class="col-sm-8">
                                            <div class="">
                                                
                                                <textarea placeholder="Enter Pembuka" id="ctrl-PEMBUKA"  rows="" name="PEMBUKA" class="htmleditor form-control"><?php  echo $this->set_field_value('PEMBUKA',''); ?></textarea>
                                                <!--<div class="invalid-feedback animated bounceIn text-center">Please enter text</div>-->
                                                
                                                
                                            </div>
                                            
                                            
                                        </div>
                                    </div>
                                </div>
                                
                                
                                
                                
                                <div class="form-group ">
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <label class="control-label" for="PASAL1">Pasal1 </label>
                                        </div>
                                        <div class="col-sm-8">
                                            <div class="">
                                                
                                                <textarea placeholder="Enter Pasal1" id="ctrl-PASAL1"  rows="" name="PASAL1" class="htmleditor form-control"><?php  echo $this->set_field_value('PASAL1',''); ?></textarea>
                                                <!--<div class="invalid-feedback animated bounceIn text-center">Please enter text</div>-->
                                                
                                                
                                            </div>
                                            
                                            
                                        </div>
                                    </div>
                                </div>
                                
                                
                                
                                
                            </div>
                            <div class="form-group form-submit-btn-holder text-center">
                                <div class="form-ajax-status"></div>
                                <button class="btn btn-primary" type="submit">
                                    
                                    <i class="fa fa-send"></i>
                                </button>
                            </div>
                        </form>
                    </div>
                    
                </div>
                
            </div>
        </div>
    </div>
    
</section>
