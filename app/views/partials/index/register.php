
<?php
	$comp_model = new SharedController;
	
	$csrf_token = Csrf :: $token;
	
	$show_header = $this->show_header;
	$view_title = $this->view_title;
	$redirect_to = $this->redirect_to;

?>

	<section class="page">
		
<?php
	if( $show_header == true ){
?>

		<div  class="bg-light p-3 mb-3">
			<div class="container">
				
				<div class="row ">
					
		<div class="col-sm-6 ">
			<h3 class="record-title">User registration</h3>

		</div>

		<div class="col-sm-6 comp-grid">
			<div class="">
	<div class="text-center">
		Already have an account?  <a class="btn btn-primary" href="<?php print_link('') ?>"> Login</a>
	</div>
</div>
		</div>

				</div>
			</div>
		</div>

<?php
	}
?>

		<div  class="">
			<div class="container">
				
				<div class="row ">
					
		<div class="col-md-7 comp-grid">
			
	<?php $this :: display_page_errors(); ?>
	
	<div  class=" animated fadeInDown">
		<form id="akun-userregister-form" role="form" novalidate enctype="multipart/form-data" class="form form-horizontal needs-validation" action="<?php print_link("index/register?csrf_token=$csrf_token") ?>" method="post">
		<div>
		
								
								<div class="form-group ">
									<div class="row">
										<div class="col-sm-4">
											<label class="control-label" for="NOMORKTP">Nomorktp <span class="text-danger">*</span></label>
										</div>
										<div class="col-sm-8">
											<div class="">
												<input id="ctrl-NOMORKTP"  value="<?php  echo $this->set_field_value('NOMORKTP',''); ?>" type="number" placeholder="Input Nomor KTP" step="1"  required="" name="NOMORKTP"  class="form-control " />
									 
 
												
											</div>
											 
											
										</div>
									</div>
								</div>
				
				

								
								<div class="form-group ">
									<div class="row">
										<div class="col-sm-4">
											<label class="control-label" for="USERNAME">Username <span class="text-danger">*</span></label>
										</div>
										<div class="col-sm-8">
											<div class="">
												<input id="ctrl-USERNAME"  value="<?php  echo $this->set_field_value('USERNAME',''); ?>" type="text" placeholder="Masukan Nama Lengkap"  required="" name="USERNAME"  class="form-control " />
									 
 
												
											</div>
											 
											
										</div>
									</div>
								</div>
				
				

								
								<div class="form-group ">
									<div class="row">
										<div class="col-sm-4">
											<label class="control-label" for="PASSWORD">Password <span class="text-danger">*</span></label>
										</div>
										<div class="col-sm-8">
											<div class="">
												<input id="ctrl-PASSWORD"  value="<?php  echo $this->set_field_value('PASSWORD',''); ?>" type="password" placeholder="Ketik Password"  required="" name="PASSWORD"  class="form-control " />
									 
 
												
											</div>
											 
											
										</div>
									</div>
								</div>
				
				

								
								<div class="form-group ">
									<div class="row">
										<div class="col-sm-4">
											<label class="control-label" for="EMAIL">Email </label>
										</div>
										<div class="col-sm-8">
											<div class="">
												<input id="ctrl-EMAIL"  value="<?php  echo $this->set_field_value('EMAIL',''); ?>" type="email" placeholder="Isikan Email Jika Ada, Kalo gak ada ya udah gak usah"  name="EMAIL"  data-url="api/json/akun_EMAIL_value_exist/" data-loading-msg="Checking availability ..." data-available-msg="Available" data-unavailable-msg="Not available" class="form-control  ctrl-check-duplicate" />
									 
<div class="check-status"></div> 
												
											</div>
											 
											
										</div>
									</div>
								</div>
				
				

								
								<div class="form-group ">
									<div class="row">
										<div class="col-sm-4">
											<label class="control-label" for="PHOTO">Photo <span class="text-danger">*</span></label>
										</div>
										<div class="col-sm-8">
											<div class="">
												
	<div class="dropzone required" id="PHOTO_upload" input="#ctrl-PHOTO" fieldname="PHOTO"    data-multiple="false" dropmsg="Masukan Foto"    btntext="Browse" extensions=".jpg,.png,.gif,.jpeg" filesize="3" maximum="1">
		<input name="PHOTO" id="ctrl-PHOTO" required="" class="dropzone-input form-control" value="<?php  echo $this->set_field_value('PHOTO',''); ?>" type="text"  />
		<!--<div class="invalid-feedback animated bounceIn text-center">Please a choose file</div>-->
		<div class="dz-file-limit animated bounceIn text-center text-danger"></div>
	</div>
 
												
											</div>
											 
											
										</div>
									</div>
								</div>
				
				


		</div>
		<div class="form-group form-submit-btn-holder text-center">
			<button class="btn btn-primary" type="submit">
				
				<i class="fa fa-send"></i>
			</button>
		</div>
	</form>
	</div>

		</div>

				</div>
			</div>
		</div>

	</section>
