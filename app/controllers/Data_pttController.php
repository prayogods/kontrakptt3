<?php 
/**
 * Data_ptt Page Controller
 * @category  Controller
 */
class Data_pttController extends SecureController{
	/**
     * Load Record Action 
     * $arg1 Field Name
     * $arg2 Field Value 
     * $param $arg1 string
     * $param $arg1 string
     * @return View
     */
	function index($fieldname = null , $fieldvalue = null){
		$db = $this->GetModel();
		$tablename = $this->tablename = 'data_ptt';
		$fields = array('ID', 
			'USERNAME', 
			'NOMORKTP', 
			'TEMPATLAHIR', 
			'TANGGALLAHIR', 
			'NOHP', 
			'ALAMAT', 
			'PENDIDIKAN', 
			'PHOTO', 
			'FOTOKTP', 
			'SERTIFIKAT', 
			'IJASAH', 
			'KK', 
			'NPWP', 
			'INSTANSI', 
			'MASAKERJA');
		$limit = $this->get_page_limit(MAX_RECORD_COUNT); // return pagination from BaseModel Class e.g array(5,20)
		if(!empty($this->search)){
			$text = trim($this->search);
			$db->orWhere('ID',"%$text%",'LIKE');
			$db->orWhere('USERNAME',"%$text%",'LIKE');
			$db->orWhere('NOMORKTP',"%$text%",'LIKE');
			$db->orWhere('EMAIL',"%$text%",'LIKE');
			$db->orWhere('TEMPATLAHIR',"%$text%",'LIKE');
			$db->orWhere('TANGGALLAHIR',"%$text%",'LIKE');
			$db->orWhere('NOHP',"%$text%",'LIKE');
			$db->orWhere('ALAMAT',"%$text%",'LIKE');
			$db->orWhere('PENDIDIKAN',"%$text%",'LIKE');
			$db->orWhere('PHOTO',"%$text%",'LIKE');
			$db->orWhere('SERTIFIKAT',"%$text%",'LIKE');
			$db->orWhere('IJASAH',"%$text%",'LIKE');
			$db->orWhere('KK',"%$text%",'LIKE');
			$db->orWhere('NPWP',"%$text%",'LIKE');
			$db->orWhere('INSTANSI',"%$text%",'LIKE');
			$db->orWhere('nomorkontrak',"%$text%",'LIKE');
			$db->orWhere('kontraklama',"%$text%",'LIKE');
			$db->orWhere('MASAKERJA',"%$text%",'LIKE');
		}
		if(!empty($this->orderby)){ // when order by request fields (from $_GET param)
			$db->orderBy($this->orderby,$this->ordertype);
		}
		else{
			$db->orderBy('data_ptt.ID', ORDER_TYPE);
		}
		if(!empty($this->groupby)){ // when group by request fields (from $_GET param)
			$db->groupBy($this->groupby);
		}
		else{
			$db->groupBy("NOMORKTP");
		}
		$allowed_roles = array ('administrator');
		if(!in_array(strtolower(USER_ROLE) , $allowed_roles)){
		$db->where("data_ptt.USERNAME" , get_active_user('USERNAME') );
		}
		if( !empty($fieldname) ){
			$db->where($fieldname , $fieldvalue);
		}
		if(!empty($_GET['data_ptt_INSTANSI'])){
			$db->where('data_ptt.INSTANSI', $_GET['data_ptt_INSTANSI'], '=');
		}
		$tc = $db->withTotalCount();
		$records = $db->get($tablename, $limit, $fields);
		$data = new stdClass;
		$data->records = $records;
		$data->record_count = count($records);
		$data->total_records = intval($tc->totalCount);
		if($db->getLastError()){
			$page_error = $db->getLastError();
			$this->view->page_error = $page_error;
		}
		$this->view->page_title ="Data Ptt";
		$this->view->render('data_ptt/list.php' , $data ,'main_layout.php');
	}
	/**
     * Load csv|json data
     * @return data
     */
	function import_data(){
		if(!empty($_FILES['file'])){
			$finfo = pathinfo($_FILES['file']['name']);
			$ext = strtolower($finfo['extension']);
			if(!in_array($ext , array('csv','json'))){
				set_flash_msg("File format not supported",'danger');
			}
			else{
			$file_path = $_FILES['file']['tmp_name'];
				if(!empty($file_path)){
					$db = $this->GetModel();
					$tablename = $this->tablename = 'data_ptt';
					if($ext == 'csv'){
						$options = array('table' => $tablename, 'fields' => '', 'delimiter' => ',', 'quote' => '"');
						$data = $db->loadCsvData( $file_path , $options , false );
					}
					else{
						$data = $db->loadJsonData( $file_path, $tablename , false );
					}
					if($db->getLastError()){
						set_flash_msg($db->getLastError(),'danger');
					}
					else{
						set_flash_msg("Data imported successfully",'success');
					}
				}
				else{
					set_flash_msg("Error uploading file",'success');
				}
			}
		}
		else{
			set_flash_msg("No file selected for upload",'warning');
		}
		$list_page = (!empty($_POST['redirect']) ? $_POST['redirect'] : 'data_ptt/list');
		redirect_to_page($list_page);
	}
	/**
     * View Record Action 
     * @return View
     */
	function view( $rec_id = null , $value = null){
		$db = $this->GetModel();
		$this->rec_id = $rec_id;
		$tablename = $this->tablename = 'data_ptt';
		$fields = array('ID', 
			'USERNAME', 
			'NOMORKTP', 
			'TEMPATLAHIR', 
			'TANGGALLAHIR', 
			'NOHP', 
			'ALAMAT', 
			'PENDIDIKAN', 
			'FOTOKTP', 
			'SERTIFIKAT', 
			'PHOTO', 
			'IJASAH', 
			'KK', 
			'NPWP', 
			'INSTANSI', 
			'nomorkontrak', 
			'kontraklama', 
			'MASAKERJA');
		$allowed_roles = array ('administrator');
		if(!in_array(strtolower(USER_ROLE) , $allowed_roles)){
		$db->where("data_ptt.USERNAME" , get_active_user('USERNAME') );
		}
		if( !empty($value) ){
			$db->where($rec_id, urldecode($value));
		}
		else{
			$db->where('data_ptt.ID' , $rec_id);
		}
		$record = $db->getOne($tablename, $fields );
		if(!empty($record)){
			$this->view->page_title ="View  Data Ptt";
			$this->view->render('data_ptt/view.php' , $record ,'main_layout.php');
		}
		else{
			$page_error = null;
			if($db->getLastError()){
				$page_error = $db->getLastError();
			}
			else{
				$page_error = "No record found";
			}
			$this->view->page_error = $page_error;
			$this->view->render('data_ptt/view.php' , $record , 'main_layout.php');
		}
	}
	/**
     * Add New Record Action 
     * If Not $_POST Request, Display Add Record Form View
     * @return View
     */
	function add(){
		if(is_post_request()){
			Csrf :: cross_check();
			$db = $this->GetModel();
			$tablename = $this->tablename = 'data_ptt';
			$fields = $this->fields = array('USERNAME','NOMORKTP','TEMPATLAHIR','TANGGALLAHIR','NOHP','ALAMAT','PENDIDIKAN','PHOTO','FOTOKTP','SERTIFIKAT','IJASAH','KK','NPWP','INSTANSI','MASAKERJA'); //insert fields
			$postdata = $this->transform_request_data($_POST);
			$this->rules_array = array(
				'USERNAME' => 'required',
				'NOMORKTP' => 'required|numeric',
				'TEMPATLAHIR' => 'required',
				'TANGGALLAHIR' => 'required',
				'NOHP' => 'required|numeric',
				'ALAMAT' => 'required',
				'PENDIDIKAN' => 'required',
				'PHOTO' => 'required',
				'FOTOKTP' => 'required',
				'INSTANSI' => 'required',
			);
			$this->sanitize_array = array(
				'USERNAME' => 'sanitize_string',
				'NOMORKTP' => 'sanitize_string',
				'TEMPATLAHIR' => 'sanitize_string',
				'TANGGALLAHIR' => 'sanitize_string',
				'NOHP' => 'sanitize_string',
				'PENDIDIKAN' => 'sanitize_string',
				'PHOTO' => 'sanitize_string',
				'FOTOKTP' => 'sanitize_string',
				'SERTIFIKAT' => 'sanitize_string',
				'IJASAH' => 'sanitize_string',
				'KK' => 'sanitize_string',
				'NPWP' => 'sanitize_string',
				'INSTANSI' => 'sanitize_string',
				'MASAKERJA' => 'sanitize_string',
			);
			$modeldata = $this -> modeldata = $this->validate_form($postdata);
			if(empty($this->view->page_error)){
				$rec_id = $this->rec_id = $db->insert($tablename, $modeldata);
				if(!empty($rec_id)){
					if(is_ajax()){
						render_json("Record added successfully");
					}
					else{
						set_flash_msg('','');
						redirect_to_page("data_ptt");
					}
					return;
				}
				else{
					$page_error = null;
					if($db->getLastError()){
						$page_error = $db->getLastError();
					}
					else{
						$page_error = "Error inserting record";
					}
					if(is_ajax()){
						render_error($page_error); 
						return;
					}
					else{
						$this->view->page_error[] = $page_error;
					}
				}
			}
		}
		$this->view->page_title ="Add New Data Ptt";
		$this->view->render('data_ptt/add.php' ,null,'main_layout.php');
	}
	/**
     * Edit Record Action 
     * If Not $_POST Request, Display Edit Record Form View
     * @return View
     */
	function edit($rec_id = null){
		$db = $this->GetModel();
		$this->rec_id = $rec_id;
		$tablename = $this->tablename = 'data_ptt';
		$fields = $this->fields = array('ID','USERNAME','NOMORKTP','TEMPATLAHIR','TANGGALLAHIR','NOHP','ALAMAT','PENDIDIKAN','PHOTO','FOTOKTP','SERTIFIKAT','IJASAH','KK','NPWP','nomorkontrak','kontraklama','INSTANSI','MASAKERJA'); //editable fields
		if(is_post_request()){
			Csrf :: cross_check();
			$postdata = $this->transform_request_data($_POST);
			$this->rules_array = array(
				'USERNAME' => 'required',
				'NOMORKTP' => 'required|numeric',
				'TEMPATLAHIR' => 'required',
				'TANGGALLAHIR' => 'required',
				'NOHP' => 'required|numeric',
				'ALAMAT' => 'required',
				'PENDIDIKAN' => 'required',
				'PHOTO' => 'required',
				'FOTOKTP' => 'required',
				'INSTANSI' => 'required',
			);
			$this->sanitize_array = array(
				'USERNAME' => 'sanitize_string',
				'NOMORKTP' => 'sanitize_string',
				'TEMPATLAHIR' => 'sanitize_string',
				'TANGGALLAHIR' => 'sanitize_string',
				'NOHP' => 'sanitize_string',
				'PENDIDIKAN' => 'sanitize_string',
				'PHOTO' => 'sanitize_string',
				'FOTOKTP' => 'sanitize_string',
				'SERTIFIKAT' => 'sanitize_string',
				'IJASAH' => 'sanitize_string',
				'KK' => 'sanitize_string',
				'NPWP' => 'sanitize_string',
				'nomorkontrak' => 'sanitize_string',
				'kontraklama' => 'sanitize_string',
				'INSTANSI' => 'sanitize_string',
				'MASAKERJA' => 'sanitize_string',
			);
			$modeldata = $this -> modeldata = $this->validate_form($postdata);
			if(empty($this->view->page_error)){
		$allowed_roles = array ('administrator');
		if(!in_array(strtolower(USER_ROLE) , $allowed_roles)){
		$db->where("data_ptt.USERNAME" , get_active_user('USERNAME') );
		}
				$db->where('data_ptt.ID' , $rec_id);
				$bool = $db->update($tablename, $modeldata);
				$numRows = $db->getRowCount(); //number of affected rows. 0 = no record field updated
				if($bool && $numRows){
					if(is_ajax()){
						render_json("Record updated successfully");
					}
					else{
						set_flash_msg("Update Berhasil",'success');
						redirect_to_page("data_ptt");
					}
					return;
				}
				else{
					$page_error = null;
					if($db->getLastError()){
						$page_error = $db->getLastError();
					}
					elseif(!$numRows){
						$page_error = "No record updated";
						if(is_ajax()){
							render_error($page_error); //return http status error
						}
						else{
							//no changes made to the table record
							set_flash_msg($page_error, 'warning');
							redirect_to_page("data_ptt");
						}
						return;
					}
					else{
						$page_error = "No record found";
					}
					if(is_ajax()){
						render_error($page_error); //return http status error
						return;
					}
					//continue to display edit page with errors
					$this->view->page_error[] = $page_error;
				}
			}
		}
		$allowed_roles = array ('administrator');
		if(!in_array(strtolower(USER_ROLE) , $allowed_roles)){
		$db->where("data_ptt.USERNAME" , get_active_user('USERNAME') );
		}
		$db->where('data_ptt.ID' , $rec_id);
		$data = $db->getOne($tablename, $fields);
		$this->view->page_title ="Edit  Data Ptt";
		if(!empty($data)){
			$this->view->render('data_ptt/edit.php' , $data, 'main_layout.php');
		}
		else{
			if($db->getLastError()){
				$this->view->page_error[] = $db->getLastError();
			}
			else{
				$this->view->page_error[] = "No record found";
			}
			$this->view->render('data_ptt/edit.php' , $data , 'main_layout.php');
		}
	}
	/**
     * Edit single field Action 
     * Return record id
     * @return View
     */
	function editfield($rec_id = null){
		$db = $this->GetModel();
		$this->rec_id = $rec_id;
		$tablename = $this->tablename = 'data_ptt';
		$fields = $this->fields = array('ID','USERNAME','NOMORKTP','TEMPATLAHIR','TANGGALLAHIR','NOHP','ALAMAT','PENDIDIKAN','PHOTO','FOTOKTP','SERTIFIKAT','IJASAH','KK','NPWP','nomorkontrak','kontraklama','INSTANSI','MASAKERJA'); //editable fields
		if(is_post_request()){
			Csrf :: cross_check();
			$postdata = array();
			if(isset($_POST['name']) && isset($_POST['value'])){
				$fieldname = $_POST['name'];
				$fieldvalue = $_POST['value'];
				$postdata[$fieldname] = $fieldvalue;
				$postdata = $this->transform_request_data($postdata);
			}
			else{
				$this->view->page_error = "invalid post data";
			}
			$this->rules_array = array(
				'USERNAME' => 'required',
				'NOMORKTP' => 'required|numeric',
				'TEMPATLAHIR' => 'required',
				'TANGGALLAHIR' => 'required',
				'NOHP' => 'required|numeric',
				'ALAMAT' => 'required',
				'PENDIDIKAN' => 'required',
				'PHOTO' => 'required',
				'FOTOKTP' => 'required',
				'INSTANSI' => 'required',
			);
			$this->sanitize_array = array(
				'USERNAME' => 'sanitize_string',
				'NOMORKTP' => 'sanitize_string',
				'TEMPATLAHIR' => 'sanitize_string',
				'TANGGALLAHIR' => 'sanitize_string',
				'NOHP' => 'sanitize_string',
				'PENDIDIKAN' => 'sanitize_string',
				'PHOTO' => 'sanitize_string',
				'FOTOKTP' => 'sanitize_string',
				'SERTIFIKAT' => 'sanitize_string',
				'IJASAH' => 'sanitize_string',
				'KK' => 'sanitize_string',
				'NPWP' => 'sanitize_string',
				'nomorkontrak' => 'sanitize_string',
				'kontraklama' => 'sanitize_string',
				'INSTANSI' => 'sanitize_string',
				'MASAKERJA' => 'sanitize_string',
			);
			$modeldata = $this -> modeldata = $this->validate_form($postdata, true);
			if(empty($this->view->page_error)){
		$allowed_roles = array ('administrator');
		if(!in_array(strtolower(USER_ROLE) , $allowed_roles)){
		$db->where("data_ptt.USERNAME" , get_active_user('USERNAME') );
		}
				$db->where('data_ptt.ID' , $rec_id);
				try{
					$bool = $db->update($tablename, $modeldata);
					$numRows = $db->getRowCount();
					if($bool && $numRows){
						render_json(
							array(
								'num_rows' =>$numRows,
								'rec_id' =>$rec_id,
							)
						);
					}
					else{
						$page_error = null;
						if($db->getLastError()){
							$page_error = $db->getLastError();
						}
						elseif(!$numRows){
							$page_error = "No record updated";
						}
						else{
							$page_error = "No record found";
						}
						render_error($page_error);
					}
				}
				catch(Exception $e){
					render_error($e->getMessage());
				}
			}
			else{
				render_error($this->view->page_error);
			}
		}
		else{
			render_error("Request type not accepted");
		}
	}
	/**
     * Delete Record Action 
     * @return View
     */
	function delete( $rec_ids = null ){
		Csrf :: cross_check();
		$db = $this->GetModel();
		$this->rec_id = $rec_ids;
		$tablename = $this->tablename = 'data_ptt';
		//split record id separated by comma into array
		$arr_id = explode(',', $rec_ids);
		//set query conditions for all records that will be deleted
		foreach($arr_id as $rec_id){
			$db->where('data_ptt.ID' , $rec_id,"=",'OR');
		}
		$allowed_roles = array ('administrator');
		if(!in_array(strtolower(USER_ROLE) , $allowed_roles)){
		$db->where("data_ptt.USERNAME" , get_active_user('USERNAME') );
		}
		$bool = $db->delete($tablename);
		if($bool){
			set_flash_msg("Berhasil di Delete",'success');
		}
		else{
			$page_error = "";
			if($db->getLastError()){
				$page_error = $db->getLastError();
			}
			else{
				$page_error = "Error deleting the record. please make sure that the record exit";
			}
			set_flash_msg($page_error,'danger');
		}
		redirect_to_page("data_ptt");
	}
}
