<?php 

/**
 * SharedController Controller
 * @category  Controller / Model
 */
class SharedController extends BaseController{
	
	/**
     * pasal_INSTANSI_option_list Model Action
     * @return array
     */
	function pasal_INSTANSI_option_list(){
		$db = $this->GetModel();
		$sqltext = "SELECT  DISTINCT INSTANSI AS value,INSTANSI AS label FROM instansi ORDER BY INSTANSI";
		$arr = $db->rawQuery($sqltext);
		return $arr;
	}

	/**
     * data_ptt_INSTANSI_option_list Model Action
     * @return array
     */
	function data_ptt_INSTANSI_option_list(){
		$db = $this->GetModel();
		$sqltext = "SELECT  DISTINCT INSTANSI AS value,INSTANSI AS label FROM instansi ORDER BY INSTANSI ASC";
		$arr = $db->rawQuery($sqltext);
		return $arr;
	}

	/**
     * akun_USERNAME_value_exist Model Action
     * @return array
     */
	function akun_USERNAME_value_exist($val){
		$db = $this->GetModel();
		$db->where('USERNAME', $val);
		$exist = $db->has('akun');
		return $exist;
	}

	/**
     * akun_EMAIL_value_exist Model Action
     * @return array
     */
	function akun_EMAIL_value_exist($val){
		$db = $this->GetModel();
		$db->where('EMAIL', $val);
		$exist = $db->has('akun');
		return $exist;
	}

	/**
     * data_ptt_data_pttINSTANSI_option_list Model Action
     * @return array
     */
	function data_ptt_data_pttINSTANSI_option_list(){
		$db = $this->GetModel();
		$sqltext = "SELECT  DISTINCT INSTANSI AS value,INSTANSI AS label FROM instansi ORDER BY INSTANSI ASC";
		$arr = $db->rawQuery($sqltext);
		return $arr;
	}

	/**
     * getcount_profilptt Model Action
     * @return Value
     */
	function getcount_profilptt(){
		$db = $this->GetModel();
		$sqltext = "SELECT COUNT(*) AS num FROM data_ptt";
		$val = $db->rawQueryValue($sqltext);
		
		if(is_array($val)){
			return $val[0];
		}
		return $val;
	}

	/**
     * getcount_jumlahseluruhptt Model Action
     * @return Value
     */
	function getcount_jumlahseluruhptt(){
		$db = $this->GetModel();
		$sqltext = "SELECT COUNT(*) AS num FROM data_ptt";
		$val = $db->rawQueryValue($sqltext);
		
		if(is_array($val)){
			return $val[0];
		}
		return $val;
	}

	/**
     * getcount_jumlahpramubakti Model Action
     * @return Value
     */
	function getcount_jumlahpramubakti(){
		$db = $this->GetModel();
		$sqltext = "SELECT COUNT(*) AS num FROM data_ptt WHERE INSTANSI='PRAMUBAKTI'";
		$val = $db->rawQueryValue($sqltext);
		
		if(is_array($val)){
			return $val[0];
		}
		return $val;
	}

	/**
     * getcount_jumlahpetugaskebersihan Model Action
     * @return Value
     */
	function getcount_jumlahpetugaskebersihan(){
		$db = $this->GetModel();
		$sqltext = "SELECT COUNT(*) AS num FROM data_ptt WHERE INSTANSI='PETUGAS KEBERSIHAN'";
		$val = $db->rawQueryValue($sqltext);
		
		if(is_array($val)){
			return $val[0];
		}
		return $val;
	}

	/**
     * getcount_jumlahsatuanpengamanan Model Action
     * @return Value
     */
	function getcount_jumlahsatuanpengamanan(){
		$db = $this->GetModel();
		$sqltext = "SELECT COUNT(*) AS num FROM data_ptt WHERE INSTANSI='SATUAN PENGAMANAN'";
		$val = $db->rawQueryValue($sqltext);
		
		if(is_array($val)){
			return $val[0];
		}
		return $val;
	}

	/**
     * getcount_jumlahpengemudi Model Action
     * @return Value
     */
	function getcount_jumlahpengemudi(){
		$db = $this->GetModel();
		$sqltext = "SELECT COUNT(*) AS num FROM data_ptt WHERE INSTANSI = 'PENGEMUDI'";
		$val = $db->rawQueryValue($sqltext);
		
		if(is_array($val)){
			return $val[0];
		}
		return $val;
	}

	/**
     * getcount_jumlahteknisi Model Action
     * @return Value
     */
	function getcount_jumlahteknisi(){
		$db = $this->GetModel();
		$sqltext = "SELECT COUNT(*) AS num FROM data_ptt WHERE INSTANSI = 'TEKNISI'";
		$val = $db->rawQueryValue($sqltext);
		
		if(is_array($val)){
			return $val[0];
		}
		return $val;
	}

}
